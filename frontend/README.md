# Pokedex

### This is a [Next.js](https://nextjs.org/) project with [Apollo Client](https://www.apollographql.com/docs/react/) as state management system and [SCSS](https://sass-lang.com/) precompiler.

## Experience

I hold more experience with ReactJs instead of NextJS. So, 2 days before when I started this exercise I had no idea about NextJS and Apollo. I acknowledge the risks of doing that, but I wanted to show some of my best skills, except coding. I can adapt very fast, and I believe in my capabilities. I hope that you will like the results of my efforts. Furthermore, it was very interesting and fun to do exercise.
