import { gql } from '@apollo/client'
import { useState, useEffect } from 'react'
import client from '../apollo/client'
import styles from '../styles/buttonFavorite.module.scss'

export default function ButtonFavorite({ pokemonId }) {
  const [isFavorite, setIsFavorite] = useState(null)

  const getIsFavorite = gql`
    query Query {
      pokemonById(id: "${pokemonId}") {
        isFavorite
      }
    }`

  const unfavorite = gql`
    mutation UnFavoritePokemonMutation($unFavoritePokemonId: ID!) {
      unFavoritePokemon(id: $unFavoritePokemonId) {
        isFavorite
      }
    }
  `

  const favorite = gql`
    mutation FavoritePokemonMutation($favoritePokemonId: ID!) {
      favoritePokemon(id: $favoritePokemonId) {
        isFavorite
      }
    }
  `

  useEffect(() => {
    client.query({ query: getIsFavorite }).then((result) => {
      const data = result.data.pokemonById

      setIsFavorite(data.isFavorite)
    })
  }, [])

  const handleOnClick = () => {
    if (isFavorite) {
      client
        .mutate({
          mutation: unfavorite,
          variables: {
            unFavoritePokemonId: pokemonId,
          },
        })
        .then(() => {
          setIsFavorite(false)
        })
    } else {
      client
        .mutate({
          mutation: favorite,
          variables: {
            favoritePokemonId: pokemonId,
          },
        })
        .then(() => {
          setIsFavorite(true)
        })
    }
  }

  return (
    <>
      <button className={styles.btnHeart} onClick={() => handleOnClick()}>
        {isFavorite ? (
          <i
            className={styles.btnHeart}
            class='fa fa-heart'
            aria-hidden='true'
          ></i>
        ) : (
          <i
            className={styles.btnHeart}
            class='fa fa-heart-broken'
            aria-hidden='true'
          ></i>
        )}
      </button>
    </>
  )
}
