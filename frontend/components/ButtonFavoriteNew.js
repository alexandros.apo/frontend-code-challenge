import { gql, useMutation, useQuery } from '@apollo/client'
import { useState, useEffect } from 'react'
import styles from '../styles/buttonFavorite.module.scss'

const getIsFavorite = gql`
  query Query($pokemonByIdId: ID!) {
    pokemonById(id: $pokemonByIdId) {
      isFavorite
    }
  }
`

const unfavorite = gql`
  mutation UnFavoritePokemonMutation($unFavoritePokemonId: ID!) {
    unFavoritePokemon(id: $unFavoritePokemonId) {
      isFavorite
    }
  }
`

const favorite = gql`
  mutation FavoritePokemonMutation($favoritePokemonId: ID!) {
    favoritePokemon(id: $favoritePokemonId) {
      isFavorite
    }
  }
`

export default function ButtonFavoriteNew({ pokemonId }) {
  const { loading, error, data } = useQuery(getIsFavorite, {
    variables: {
      pokemonByIdId: pokemonId,
    },
  })
  const [isFavorite, setIsFavorite] = useState(null)
  const [execFavorite] = useMutation(favorite)
  const [execUnfavorite] = useMutation(unfavorite)

  if (loading) return <p>...</p>
  if (error) {
    console.error(error)
    return null
  }

  const handleOnClick = () => {
    if (isFavorite) {
      execUnfavorite({
        variables: { unFavoritePokemonId: pokemonId },
      }).then((result) => {
        if (result.data) {
          setIsFavorite(false)
        }
      })
    } else {
      execFavorite({
        variables: { favoritePokemonId: pokemonId },
      }).then((result) => {
        if (result.data) {
          setIsFavorite(true)
        }
      })
    }
  }

  useEffect(() => {
    data && data.pokemonById && setIsFavorite(data.pokemonById.isFavorite)
  }, [data])

  return (
    <>
      <button className={styles.btnHeart} onClick={() => handleOnClick()}>
        {isFavorite ? (
          <i
            className={styles.btnHeart}
            class='fa fa-heart'
            aria-hidden='true'
          ></i>
        ) : (
          <i
            className={styles.btnHeart}
            class='fa fa-heart-broken'
            aria-hidden='true'
          ></i>
        )}
      </button>
    </>
  )
}
