import { useQuery, gql, NetworkStatus } from '@apollo/client'
import styles from '../styles/navbar.module.scss'
import Pokemons from '../components/Pokemons'
import { useState } from 'react'

const QUERY = gql`
  query Query {
    pokemonTypes
  }
`

export default function Navbar() {
  const { data, loading, error } = useQuery(QUERY)
  const [isGrid, setIsGrid] = useState(true)
  const [showFavorites, setShowFavorites] = useState(false)
  const [pokType, setPokType] = useState('All')
  const [filtering, setFiltering] = useState('')

  const handleFilterChange = (e) => {
    setFiltering(e.target.value)
  }

  const handleLinksClick = (toggle) => {
    setShowFavorites(toggle)
  }

  const handleLayoutClick = (toggle) => {
    setIsGrid(toggle)
  }

  const handleTypeChange = (e) => {
    setPokType(e.target.value)
  }

  if (loading) {
    return <h2>Loading...</h2>
  }

  if (error) {
    console.error(error)
    return null
  }

  return (
    <>
      <nav className={styles.navbar}>
        <div className={styles.navbar_links}>
          <button
            className={
              showFavorites
                ? styles.navbar__button
                : styles.navbar__buttonActive
            }
            onClick={() => handleLinksClick(false)}
          >
            All
          </button>
          <button
            className={
              !showFavorites
                ? styles.navbar__button
                : styles.navbar__buttonActive
            }
            onClick={() => handleLinksClick(true)}
          >
            Favorites
          </button>
        </div>
        <div className={styles.navbar__tools}>
          <div className={styles.navbar__search}>
            <input
              type='text'
              placeholder='Search'
              onChange={(e) => handleFilterChange(e)}
            />
          </div>
          <div className={styles.navbar__filter}>
            <select name='option' onChange={(e) => handleTypeChange(e)}>
              <option key={0} value='All' selected>
                All
              </option>
              {data.pokemonTypes.map((type, index) => (
                <option key={index} value={type}>
                  {type}
                </option>
              ))}
            </select>
          </div>
          <div className={styles.navbar__switch}>
            <button
              className={styles.navbar__SwitchList}
              onClick={() => handleLayoutClick(false)}
            >
              <i class='fa fa-bars' aria-hidden='true'></i>
            </button>
            <button
              className={styles.navbar__SwitchGrid}
              onClick={() => handleLayoutClick(true)}
            >
              <i class='fa fa-th' aria-hidden='true'></i>
            </button>
          </div>
        </div>
      </nav>
      <Pokemons
        isGrid={isGrid}
        showFavorites={showFavorites}
        pokType={pokType}
        filtering={filtering}
      />
    </>
  )
}
