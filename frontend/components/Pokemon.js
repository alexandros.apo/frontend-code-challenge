import { useQuery, gql } from '@apollo/client'
import { useState, useEffect } from 'react'
import Image from 'next/image'
import Link from 'next/link'
import Router from 'next/router'
import styles from '../styles/pokemon.module.scss'
import ButtonFavorite from '../components/ButtonFavorite'
import ButtonFavoriteNew from '../components/ButtonFavoriteNew'
import classNames from 'classNames'

export default function Pokemon({ pokemonId, isGrid }) {
  const QUERY = gql`
    query Query {
      pokemonById(id: "${pokemonId}") {
        id,
        name,
        image,
        types,
        isFavorite
      }
    }`

  const { data, loading, error } = useQuery(QUERY)

  if (loading) {
    return <h2>Loading...</h2>
  }

  if (error) {
    console.error(error)
    return null
  }

  const pokemon = data.pokemonById

  return (
    data &&
    data.pokemonById && (
      <article
        className={
          isGrid
            ? classNames(styles.card)
            : classNames(styles.card, styles.cardList)
        }
      >
        <Link key={pokemon.id} as={`/${pokemon.name}`} href='/[pokemon]'>
          <div className={styles.card__img}>
            <Image
              src={pokemon.image}
              alt={pokemon.name}
              width={120}
              height={120}
              // layout='responsive'
            />
          </div>
        </Link>
        <div className={styles.card__bottom}>
          <div className={styles.card__annot}>
            <Link key={pokemon.id} as={`/${pokemon.name}`} href='/[pokemon]'>
              <h2 className={styles.card__title}>{pokemon.name}</h2>
            </Link>
            <p className={styles.card__types}>{pokemon.types.join(', ')}</p>
          </div>
          {/* <ButtonFavorite pokemonId={pokemon.id} /> */}
          <ButtonFavoriteNew pokemonId={pokemon.id} />
        </div>
      </article>
    )
  )
}
