import { useQuery, gql } from '@apollo/client'
import { useState, useEffect } from 'react'
import Image from 'next/image'
import Link from 'next/link'
import Router from 'next/router'
import styles from '../styles/pokemonEvolution.module.scss'
import ButtonFavorite from './ButtonFavorite'
import ButtonFavoriteNew from '../components/ButtonFavoriteNew'

export default function PokemonEvolution({ pokemonId }) {
  const QUERY = gql`
    query Query {
      pokemonById(id: "${pokemonId}") {
        id,
        name,
        image,
        types,
        isFavorite
      }
    }`

  const { data, loading, error } = useQuery(QUERY)

  if (loading) {
    return <h2>Loading...</h2>
  }

  if (error) {
    console.error(error)
    return null
  }

  const pokemon = data.pokemonById

  return (
    data &&
    data.pokemonById && (
      <article className={styles.card}>
        <Link key={pokemon.id} as={`/${pokemon.name}`} href='/[pokemon]'>
          <div className={styles.card__img}>
            <Image
              src={pokemon.image}
              alt={pokemon.name}
              width={100}
              height={100}
              layout='responsive'
            />
          </div>
        </Link>
        <div className={styles.card__bottom}>
          <div className={styles.card__annot}>
            <Link key={pokemon.id} as={`/${pokemon.name}`} href='/[pokemon]'>
              <h2 className={styles.card__title}>{pokemon.name}</h2>
            </Link>
          </div>
          {/* <ButtonFavorite pokemonId={pokemon.id} /> */}
          <ButtonFavoriteNew pokemonId={pokemon.id} />
        </div>
      </article>
    )
  )
}
