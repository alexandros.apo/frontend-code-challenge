import { useQuery, gql, NetworkStatus } from '@apollo/client'
import { useState, useEffect } from 'react'
import Image from 'next/image'
import Link from 'next/link'
import styles from '../styles/index.module.scss'
import Pokemon from '../components/Pokemon'

const QUERY = gql`
  query Query {
    pokemons(query: { limit: 300 }) {
      edges {
        id
        name
        types
        image
        isFavorite
      }
    }
  }
`

export default function Pokemons({
  isGrid,
  showFavorites,
  pokType,
  filtering,
}) {
  const { data, loading, error, refetch, networkStatus } = useQuery(QUERY)

  if (networkStatus === NetworkStatus.refetch) {
    return <h2>Refetching!</h2>
  }
  if (loading) {
    return <h2>Loading...</h2>
  }

  if (error) {
    console.error(error)
    return null
  }

  let pokemons = showFavorites
    ? data.pokemons.edges.filter((val) => {
        return val.isFavorite === true
      })
    : data.pokemons.edges

  if (pokType !== 'All') {
    console.log(pokType.toString())
    pokemons = pokemons.filter((val) => {
      return val.types.includes(pokType) === true
    })
  }

  if (filtering !== '') {
    pokemons = pokemons.filter((val) => {
      return val.name.toLowerCase().includes(filtering.toLowerCase()) === true
    })
  }

  return (
    <div
      className={isGrid ? `${styles.containerGrid}` : `${styles.containerList}`}
      onClick={() => refetch()}
    >
      {pokemons.map((pokemon) => (
        <Pokemon key={pokemon.id} pokemonId={pokemon.id} isGrid={isGrid} />
      ))}
    </div>
  )
}
