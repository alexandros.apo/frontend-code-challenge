import { useQuery, gql } from '@apollo/client'
import { useRouter } from 'next/router'
import Link from 'next/link'
import Image from 'next/image'
import styles from '../styles/pokemonDetails.module.scss'
import ButtonFavorite from '../components/ButtonFavorite'
import ButtonFavoriteNew from '../components/ButtonFavoriteNew'
import PokemonEvolution from '../components/PokemonEvolution'
import { useState } from 'react'

const QUERY = gql`
  query Query($pokemonByNameName: String!) {
    pokemonByName(name: $pokemonByNameName) {
      id
      name
      image
      classification
      types
      maxCP
      maxHP
      weight {
        minimum
        maximum
      }
      height {
        minimum
        maximum
      }
      isFavorite
      evolutions {
        id
      }
    }
  }
`

const PokemonItem = () => {
  const [displaySound, setDisplaySound] = useState(false)
  const router = useRouter()
  const { pokemon } = router.query

  const { data, loading, error } = useQuery(QUERY, {
    variables: {
      pokemonByNameName: pokemon,
    },
  })

  if (loading) {
    return <h2>Loading...</h2>
  }

  if (error) {
    console.error(error)
    return null
  }

  const pokemonObj = data.pokemonByName

  const soundUrl = `../sounds/${parseInt(pokemonObj.id).toString()}.mp3`
  // const audioTune = new Audio(soundUrl)

  const handlePlayOnClick = () => {
    //audioTune.play()

    setDisplaySound(!displaySound)
  }

  return (
    data &&
    data.pokemonByName && (
      <>
        <div className={styles.container}>
          <div className={styles.card__container}>
            <div className={styles.card}>
              <Link href='/'>
                <button className={styles.card__arrowBack}>
                  <i class='fa fa-arrow-left' aria-hidden='true'></i>
                </button>
              </Link>
              <button
                className={styles.card__playSound}
                onClick={() => handlePlayOnClick()}
              >
                <i class='fa fa-volume-up' aria-hidden='true'></i>
              </button>
              {displaySound && (
                <p className={styles.card__soundText}>Grrrrrr</p>
              )}
              <div className={styles.card__img}>
                <Image
                  src={pokemonObj.image}
                  alt='Vercel Logo'
                  width={350}
                  height={350}
                  layout='responsive'
                />
              </div>
              <div className={styles.card__detailsContainer}>
                <div className={styles.card__details}>
                  <div className={styles.card__title}>
                    <h1>{pokemonObj.name}</h1>
                    <p>{pokemonObj.types.join(', ')}</p>
                  </div>
                  {/* <ButtonFavorite pokemonId={pokemonObj.id} /> */}
                  <ButtonFavoriteNew pokemonId={pokemonObj.id} />
                </div>
                <div className={styles.card__bars}>
                  <div className={styles.card__barCp}>
                    <div class={styles.card__progressBarContainer}>
                      <div class={styles.card__progressBarValue}></div>
                    </div>
                    <p>CP:{pokemonObj.maxCP}</p>
                  </div>
                  <div className={styles.card__barHp}>
                    <div class={styles.card__progressBarContainer}>
                      <div class={styles.card__progressBarValue}></div>
                    </div>
                    <p>HP:{pokemonObj.maxHP}</p>
                  </div>
                </div>
                <div className={styles.card__bottom}>
                  <div className={styles.card__weight}>
                    <h2>Weight</h2>
                    <p>
                      {pokemonObj.weight.minimum} - {pokemonObj.weight.maximum}
                    </p>
                  </div>
                  <div className={styles.card__height}>
                    <h2>Height</h2>
                    <p>
                      {pokemonObj.height.minimum} - {pokemonObj.height.maximum}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <h2 className={styles.card__evolutionsTitle}>Evolutions</h2>
          <div className={styles.card__evolutions}>
            {pokemonObj.evolutions.map((evol) => (
              <PokemonEvolution key={evol.id} pokemonId={evol.id} />
            ))}
          </div>
        </div>
      </>
    )
  )
}

export default PokemonItem
