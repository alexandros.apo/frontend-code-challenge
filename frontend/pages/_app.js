import '../styles/normalize.css'
import { ApolloProvider } from '@apollo/client'
import client from '../apollo/client'
import '@fortawesome/fontawesome-free/css/all.css'

function MyApp({ Component, pageProps }) {
  return (
    <ApolloProvider client={client}>
      <Component {...pageProps} />
    </ApolloProvider>
  )
}

export default MyApp
